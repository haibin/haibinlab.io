---
title: tcpKeepAliveListener in go
date: 2017-12-25
tags: ["go"]
---

We can quickly start a http server in go as below.

```
http.ListenAndServe(":8080", nil)
```

It is called `ListenAndServe` because it creates a TCP listener and passes it to the http server.

```
ln, err := net.Listen("tcp", addr)
srv.Serve(ln)
```

The actual implementation in the go standard library uses a `tcpKeepAliveListener` which wraps the TCP listener.

```
srv.Serve(tcpKeepAliveListener{ln.(*net.TCPListener)})
```

`tcpKeepAliveListener` implements the TCP keep-alives which might not be very important because HTTP keep-alives are enabled by default.

```
SetKeepAlivesEnabled controls whether HTTP keep-alives are enabled. 
By default, keep-alives are always enabled.
```

