---
title: Use Different Git Emails for Different Repositories
date: 2017-12-08
tags: ["git"]
---

Sometimes we make mistake by using `personal` email in the git commit for `company` repositories or vice versa. To fix this issue, use user.useConfigOnly.

```
user.useConfigOnly
           
Instruct Git to avoid trying to guess defaults for user.email and user.name, 
and instead retrieve the values only from the configuration. 
For example, if you have multiple email addresses and would like to use a different one for each repository, 
then with this configuration option set to true in the global config along with a name, 
Git will prompt you to set up an email before making new commits in a newly cloned repository. 
Defaults to false.
```

First unset user email globally.

```
$ git config --global --unset-all user.email
```

Set user.useConfigOnly to true.

```
$ git config --global user.useConfigOnly true
```

Then in each repository when you make commits, you will be prompted to set your email. Run the following command to set user email for the repository.

```
git config user.email "you@example.com"
```
