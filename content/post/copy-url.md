---
title: How to Make a Copy of URL in go
date: 2017-12-12
tags: ["go", "go-kit"]
---

An interesting example of making a copy of URL in go
https://github.com/go-kit/kit/blob/master/examples/addsvc/pkg/addtransport/http.go#L123

```
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
```
