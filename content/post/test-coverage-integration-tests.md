---
title: Test Coverage with Integration Tests in go
date: 2017-12-27
tags: ["go"]
---

# Basics

`go test` tests `packages` as it is stated below.

```
> go test -h
test [build/test flags] [packages] [build/test flags & test binary flags]
```

So given a package, `go test` runs both `in package` tests and `outside package` tests.

```
> go test github.com/haibin/coverage/person
ok  	github.com/haibin/coverage/person	0.007s
```

To list `in package` tests, run the following.

```
> go list -f '{{.TestGoFiles}}' github.com/haibin/coverage/person
[foo_test.go]
```

To list `outside package` tests, run the following.

```
> go list -f '{{.XTestGoFiles}}' github.com/haibin/coverage/person
[person_test.go]
```

# Test Coverage

To show a summary of test coverage, run the following

```
> go test -cover github.com/haibin/coverage/person
ok  	github.com/haibin/coverage/person	0.005s	coverage: 75.0% of statements
```

According to the `go test` doc on `-coverpkg`, `The default is for each test to analyze only the package being tested.` so the above command is the same as the below.

```
> go test -coverpkg github.com/haibin/coverage/person github.com/haibin/coverage/person
ok  	github.com/haibin/coverage/person	0.006s	coverage: 75.0% of statements in github.com/haibin/coverage/person
```

Now we have some integration tests outside of the package `github.com/haibin/coverage/person` but we want to see how much more coverage those tests provide to the same package.

```
> go test -coverpkg github.com/haibin/coverage/person github.com/haibin/coverage/it
warning: no packages being tested depend on github.com/haibin/coverage/person
ok  	github.com/haibin/coverage/it	0.006s	coverage: 25.0% of statements in github.com/haibin/coverage/person
```

We can run them together as below.

```
> go test -coverpkg github.com/haibin/coverage/person github.com/haibin/coverage/it github.com/haibin/coverage/person
ok  	github.com/haibin/coverage/it	0.005s	coverage: 25.0% of statements in github.com/haibin/coverage/person
ok  	github.com/haibin/coverage/person	0.005s	coverage: 75.0% of statements in github.com/haibin/coverage/person
```

So what is the total coverage? There might be some overlapping of tests in github.com/haibin/coverage/it and tests in github.com/haibin/coverage/person. Let's generate coverage profile.

```
> go test -coverprofile=coverage.out -coverpkg github.com/haibin/coverage/person github.com/haibin/coverage/it github.com/haibin/coverage/person
cannot use test profile flag with multiple packages
```

We got this `cannot use test profile flag with multiple packages` error. So we have to run it one by one.

```
> go test -coverprofile=it.out -coverpkg github.com/haibin/coverage/person github.com/haibin/coverage/it
warning: no packages being tested depend on github.com/haibin/coverage/person
ok  	github.com/haibin/coverage/it	0.006s	coverage: 25.0% of statements in github.com/haibin/coverage/person
```

```
> go test -coverprofile=person.out -coverpkg github.com/haibin/coverage/person github.com/haibin/coverage/person
ok  	github.com/haibin/coverage/person	0.006s	coverage: 75.0% of statements in github.com/haibin/coverage/person
```

Then we merge the two profiles.

```
> github.com/wadey/gocovmerge
> gocovmerge person.out it.out > coverage.out
```

And we get the total coverage!

```
> go tool cover -func=coverage.out
github.com/haibin/coverage/person/person.go:4:	Age		100.0%
github.com/haibin/coverage/person/person.go:9:	Name		100.0%
github.com/haibin/coverage/person/person.go:14:	City		100.0%
github.com/haibin/coverage/person/person.go:18:	foo		100.0%
total:						(statements)	100.0%
```