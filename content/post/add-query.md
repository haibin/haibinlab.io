---
title: How to Add a query to URL in go
date: 2017-12-12
tags: ["go"]
---

```
	wURL, _ := url.Parse(str)
	q := wURL.Query()
	q.Add("q", "abc")
	wURL.RawQuery = q.Encode()
```
